var app = angular.module('vehicleTypes', ['ngMaterial']);
app.controller('vehicleTypesCtrl', function($scope, $http, $window) {
    $scope.newType = function(){
        var name = prompt("Vehicle type");
        var price_base = prompt("Basic fare");
        var price_km = prompt("Fare/km");
        $http.post("http://cc2.dev.targettaxi.com/vehicleType",{"name": name, "price_base": price_base, "price_km": price_km})
            .then(function onSuccess() {
                $window.alert("New Type Added! :)");
                $window.location.reload();
            }, function onFail() {
                $window.alert("New Type Failed! :(");
            });
    }
    $scope.delete = function(id){
        $http.delete("http://cc2.dev.targettaxi.com/vehicleType/" + id)
            .then(function onSuccess() {
                $window.alert("Delete Successful! :)");
                $window.location.reload();
            }, function onFail() {
                $window.alert("Delete Failed! :(");
            });
    }
    $scope.edit =function(id,price_base,price_km) {

        var price_base = prompt("Basic fare");
        var price_km = prompt("Fare/km");
        $http.put("http://cc2.dev.targettaxi.com/vehicleType/" + id, {"price_base": price_base, "price_km": price_km})
            .then(function onSuccess() {
                $window.alert("Edit Successful! :)");
                $window.location.reload();
            }, function onFail() {
                $window.alert("Edit Failed! :(");
            });
    }

    $http.defaults.headers.common.Authorization = 'Basic ZmdyYXBzYXMrMDAyQGRnYS5ncjoxMjM0NTY=';
    $http.get("http://cc2.dev.targettaxi.com/vehicleType")
        .then(function onSuccess(response) {
        $scope.GETdata = response.data;
    }, function onFail(response) {
        $scope.test = response.statusText;
        $scope.statusCode= response.status;
        $scope.statusText = response.statusText;
        $scope.test = "Smth went wrong! :(";


    });



});
